var Express = require('express');
var path = require('path');
var compression = require('compression');
var fs = require('node-fs');

var serverConfig = {
  port: process.env.PORT || 8081
};

var app = new Express();

var oneDay = 86400000;

console.log(path.resolve(__dirname, './build/static'));

app.use(Express.static(path.resolve(__dirname, './build'), { maxAge: oneDay }));
app.use(compression());

app.get('*', function(req, res) {
  res.sendFile(path.resolve(__dirname, './build/index.html'));
});

app.listen(serverConfig.port, function(error) {
  if (!error) {
    console.log(`Todo app is running on port: ${serverConfig.port}! 🍻`);
  }
});

module.exports = app;
