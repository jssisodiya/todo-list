import React, { Component } from 'react';

// Icons
import Icon1 from '../icons/ic_inbox_white_18px.svg';
import Icon2 from '../icons/ic_add_to_queue_white_18px.svg';
import Icon3 from '../icons/ic_assignment_ind_white_18px.svg';
import Icon4 from '../icons/ic_backup_white_18px.svg';
import Icon5 from '../icons/ic_build_white_18px.svg';
import Icon6 from '../icons/ic_fiber_new_white_18px.svg';
import Icon7 from '../icons/ic_grade_white_18px.svg';
import Icon8 from '../icons/ic_settings_white_18px.svg';
import Icon9 from '../icons/ic_view_list_white_18px.svg';
import Icon10 from '../icons/ic_visibility_white_18px.svg';

class ListIcon extends Component {
	render() {
		const index = this.props.index;
		const listIndex = index % 10;

		const iconNodes = [
			<img
				alt={'Icon1'}
				src={Icon1}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon2'}
				src={Icon2}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon3'}
				src={Icon3}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon4'}
				src={Icon4}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon5'}
				src={Icon5}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon6'}
				src={Icon6}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon7'}
				src={Icon7}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon8'}
				src={Icon8}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon9'}
				src={Icon9}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>,
			<img
				alt={'Icon10'}
				src={Icon10}
				width={this.props.width || 24}
				height={this.props.width || 24}
			/>
		];
		return <div>{iconNodes[listIndex]}</div>;
	}
}

export default ListIcon;
