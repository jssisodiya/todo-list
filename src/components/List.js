import React, { Component } from 'react';

// Lodash functions
import _size from 'lodash/size';
import _filter from 'lodash/filter';

// Components
import ListIcon from './ListIcon';

// Utils
import utils from '../utils';

class List extends Component {
	render() {
		const list = this.props.list || {};
		const listTodos = this.props.listTodos || [];
		const dueTodos = _filter(
			listTodos,
			t =>
				!t.is_completed &&
				utils.formatDate(t.due_date) <= utils.formatDate(new Date())
		);
		return (
			<a
				className={`todo-list flex-container`}
				style={{ background: this.props.isActive ? list.color : 'initial' }}
				href="javascript:void(0)"
				onClick={
					this.props.handleListClick &&
					this.props.handleListClick.bind(this, list)
				}>
				<div
					style={{ width: '20px', height: '100%', background: list.color }}
				/>
				<div className="p-x-16">
					<ListIcon index={this.props.listIndex} height={54} width={54} />
				</div>
				<div className="p-4">
					<div className="todo-list-title">{list.title}</div>
					<div className="todo-list-counts">
						<span className="text-gray">
							{_size(listTodos)} {utils.pluralize('task', _size(listTodos))},
						</span>
						&nbsp;
						<span className="text-error">{_size(dueTodos)} overdue</span>
					</div>
				</div>
			</a>
		);
	}
}

export default List;
