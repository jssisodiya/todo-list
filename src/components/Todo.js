import React, { Component } from 'react';

// Icons
import SquareIcon from '../icons/ic_crop_square_black_18px.svg';
import DoneIcon from '../icons/ic_done_black_18px.svg';
import ListIcon from '../icons/ic_list_black_18px.svg';

// Utils
import utils from '../utils';

class Todo extends Component {
	state = { showDone: false };
	handleDone = todo => {
		this.setState({ showDone: true }, () => {
			if (todo.is_completed) {
				this.props.handleDone && this.props.handleDone(todo);
			} else {
				setTimeout(() => {
					this.props.handleDone && this.props.handleDone(todo);
				}, 1000);
			}
		});
	};
	render() {
		const todo = this.props.todo;
		return (
			<div className="todo flex-container flex-v-center">
				<div className="p-x-16">
					<img
						alt="done"
						onClick={this.handleDone.bind(this, todo)}
						className="pointer"
						src={
							todo.is_completed || this.state.showDone ? DoneIcon : SquareIcon
						}
						width={32}
						height={32}
					/>
				</div>
				<div className="todo-content">
					<div className="flex-container flex-v-center">
						<div className="todo-title">{todo.title}</div>
						<div className="text-gray flex-pull-right">
							{utils.formatDate(todo.created_at)}
						</div>
					</div>
					<div className="todo-description text-gray-dark flex-container flex-v-center">
						<img alt="list" src={ListIcon} width={18} height={18} />
						{todo.description}
					</div>
					<div style={{ marginTop: '16px' }} className="text-error">
						Due by: {todo.due_date}
					</div>
				</div>
			</div>
		);
	}
}

export default Todo;
