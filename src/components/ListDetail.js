import React, { Component } from 'react';

// Lodash functions
import _map from 'lodash/map';
import _filter from 'lodash/filter';
import _orderBy from 'lodash/orderBy';
import _size from 'lodash/size';

// Icons
import AddIcon from '../icons/ic_add_white_18px.svg';
import ListIcon from './ListIcon';

// Components
import Todo from './Todo';

// Utils
import utils from '../utils';

class ListDetail extends Component {
	render() {
		const { todos, list, listIndex } = this.props;
		const listColor = utils.getListColor(listIndex);
		const futureTodos = _orderBy(
			_filter(todos, t => !t.is_completed),
			'created_at',
			'desc'
		);
		const completedTodos = _orderBy(
			_filter(todos, t => {
				return t.is_completed;
			}),
			'created_at',
			'desc'
		);
		return (
			<div>
				<div
					className="list-header flex-container flex-v-center"
					style={{ background: listColor }}>
					<div className="p-x-16">
						<ListIcon index={this.props.listIndex} width={54} height={54} />
					</div>
					<div className="flex-expand list-detail-title">{list.title}</div>
					<a
						title="Add new todo in this list"
						href="javascript:void(0)"
						onClick={
							this.props.handleNewTodoClick &&
							this.props.handleNewTodoClick.bind(this)
						}>
						<img alt="add todo" src={AddIcon} width={54} height={54} />
					</a>
				</div>
				<div>
					<div className="todo-section">Future</div>
					{_size(futureTodos) === 0 &&
						_size(todos) === 0 && (
							<h3 className="p-16">There are no tasks. Please create one.</h3>
						)}
					{_map(futureTodos, todo => (
						<Todo
							key={`todo:future:${todo.id}`}
							handleDone={this.props.handleDone}
							todo={todo}
						/>
					))}
				</div>
				{_size(completedTodos) > 0 && (
					<div>
						<div className="todo-section">Completed</div>
						{_map(completedTodos, todo => (
							<Todo
								key={`todo:done:${todo.id}`}
								handleDone={this.props.handleDone}
								todo={todo}
							/>
						))}
					</div>
				)}
			</div>
		);
	}
}

export default ListDetail;
