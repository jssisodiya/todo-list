import React, { Component } from 'react';

// Lodash functions
import _map from 'lodash/map';
import _filter from 'lodash/filter';

// Components
import List from './List';

// Utils
import utils from '../utils';

class TodoLists extends Component {
  render() {
    let lists = this.props.lists || [];
    const todos = this.props.todos || [];
    return (
      <div>
        {_map(lists, (list, _index) => {
          const listTodos = _filter(todos, t => t.list_id === list.id);
          const listColor = utils.getListColor(_index);
          list.color = listColor;
          const isActive = list.id === this.props.activeListId;
          return (
            <List
              key={`list:${list.id}`}
              handleListClick={this.props.handleListClick}
              list={list}
              listTodos={list.id === 'list_all' ? todos : listTodos}
              isActive={isActive}
              listIndex={_index}
            />
          );
        })}
      </div>
    );
  }
}

export default TodoLists;
