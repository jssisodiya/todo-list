import React, { Component } from 'react';
import { SkyLightStateless } from 'react-skylight';

// CSS
import './App.css';

// Lodash Functions
import _size from 'lodash/size';
import _find from 'lodash/find';
import _findIndex from 'lodash/findIndex';
import _filter from 'lodash/filter';
import _random from 'lodash/random';
import _concat from 'lodash/concat';

// Utils
import lsUtils from './utils/LocalStorageUtils';

// Components
import TodoLists from './components/TodoLists';
import ListDetail from './components/ListDetail';

// Icons
import AddIcon from './icons/ic_add_white_18px.svg';

class App extends Component {
  state = {
    lists: [],
    todos: [],
    activeListId: 'list_all',
    showCreateList: false,
    showCreateTodo: false
  };
  componentDidMount() {
    this.fetchData();
  }
  fetchData() {
    const lists = lsUtils.get('lists') || [];
    const todos = lsUtils.get('todos') || [];
    let activeListId = this.state.activeListId;
    if (_size(lists) > 0 && !activeListId) {
      activeListId = lists[0].id;
    }
    this.setState({
      lists,
      todos,
      activeListId,
      showCreateTodo: false,
      showCreateList: false
    });
  }
  handleListClick = list => {
    console.log('handleListClick', list);
    if (list) {
      this.setState({ activeListId: list.id });
    }
  };
  addTodoList = e => {
    e.preventDefault();
    const lists = this.state.lists || [];
    lists.push({
      id: `list_${_random(0, 1000)}`,
      title: this.refs['list_title'].value
    });
    lsUtils.set('lists', lists);
    this.fetchData();
  };
  handleDone = todo => {
    console.log('handleDone', todo);
    if (todo && todo.id) {
      let todos = this.state.todos || [];
      console.log('todos', todos);
      let index = _findIndex(todos, t => t.id === todo.id);
      console.log('index', index);
      if (index > -1) {
        todo.is_completed = !todo.is_completed;
        todos[index] = todo;
        lsUtils.set('todos', todos);
        this.fetchData();
      }
    }
  };
  handleAddTodoClick = () => {
    this.setState({ showCreateTodo: true });
  };
  hideAddTodo = () => {
    this.setState({ showCreateTodo: false });
  };
  addTodo = e => {
    console.log('addTodo', e);
    e.preventDefault();
    if (!this.state.activeListId) {
      return;
    }
    const todos = this.state.todos || [];
    todos.push({
      id: `'todo_${_random(2000, 3000)}`,
      title: this.refs['todo_title'].value,
      description: this.refs['todo_description'].value,
      due_date: this.refs['due_date'].value,
      created_at: new Date(),
      list_id: this.state.activeListId,
      is_completed: false
    });
    lsUtils.set('todos', todos);
    this.fetchData();
  };
  _renderCreateTodoForm = () => {
    return (
      <div className="p-y-8">
        <form onSubmit={this.addTodo}>
          <label>Title</label>
          <input
            required
            placeholder="What do you want to achieve today?"
            type="text"
            name="todo_title"
            ref="todo_title"
          />
          <label>Description</label>
          <input
            required
            placeholder="Please provide some more information"
            type="text"
            name="todo_description"
            ref="todo_description"
          />
          <label>Due Date</label>
          <input required name="due_date" type="date" ref="due_date" />
          <button className="btn" type="submit">
            Submit
          </button>
        </form>
      </div>
    );
  };
  _renderCreateListForm = () => {
    return (
      <div className="p-y-8">
        <form onSubmit={this.addTodoList}>
          <label>Title</label>
          <input
            required
            placeholder="Name your list. e.g. Groceries"
            type="text"
            name="list_title"
            ref="list_title"
          />
          <button className="btn" type="submit">
            Submit
          </button>
        </form>
      </div>
    );
  };
  render() {
    let { lists, todos, activeListId } = this.state;
    lists = _concat({ id: 'list_all', title: 'All Tasks' }, lists);
    let activeListIndex = 0;
    let activeList = {};
    let listTodos = [];
    if (activeListId) {
      activeListIndex = _findIndex(lists, l => l.id === activeListId);
      activeList = _find(lists, l => l.id === activeListId);
      listTodos = _filter(todos, todo => todo.list_id === activeListId);
    }
    return (
      <div>
        <main>
          <aside>
            <nav>
              <div className="nav-content">
                <TodoLists
                  handleListClick={this.handleListClick}
                  lists={lists}
                  todos={todos}
                  activeListId={activeListId}
                  listIndex={activeListIndex}
                />
                <a
                  onClick={() => {
                    this.setState({ showCreateList: true });
                  }}
                  href="javascript:void(0)"
                  className="add-list flex-container flex-v-center flex-h-center">
                  <img
                    alt="add new list"
                    title={'Add new list'}
                    src={AddIcon}
                    width={32}
                    height={32}
                  />
                  Add New List
                </a>
              </div>
            </nav>
          </aside>
          <section>
            <div className="list-container">
              {activeListId && (
                <ListDetail
                  handleDone={this.handleDone}
                  handleNewTodoClick={this.handleAddTodoClick}
                  list={activeList}
                  todos={activeList.id === 'list_all' ? todos : listTodos}
                  listIndex={activeListIndex}
                />
              )}
            </div>
          </section>
        </main>
        <SkyLightStateless
          onCloseClicked={this.hideAddTodo}
          isVisible={this.state.showCreateTodo}
          ref={ref => (this.todoDialog = ref)}
          title="Add New Todo">
          {this._renderCreateTodoForm()}
        </SkyLightStateless>
        <SkyLightStateless
          dialogStyles={{ height: '200px' }}
          onCloseClicked={() => {
            this.setState({ showCreateList: false });
          }}
          isVisible={this.state.showCreateList}
          ref={ref => (this.listDialog = ref)}
          title="Add New List">
          {this._renderCreateListForm()}
        </SkyLightStateless>
      </div>
    );
  }
}

export default App;
