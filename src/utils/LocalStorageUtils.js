import lockr from 'lockr';

class LocalStorageUtils {
	static set(key, data) {
		try {
			lockr.set(key, data);
		} catch (err) {
			console.error(err.message);
		}
	}
	static get(key) {
		try {
			return lockr.get(key);
		} catch (err) {
			console.error(err.message);
		}
	}
}

export default LocalStorageUtils;
