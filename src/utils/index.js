const listColors = [
	'#E65100',
	'#FF6F00',
	'#263238',
	'#212121',
	'#BF360C',
	'#3E2723',
	'#F57F17',
	'#1B5E20',
	'#827717',
	'#33691E'
];

class Utils {
	static getListColor(index) {
		return listColors[index % 10];
	}
	static pluralize(word, count) {
		if (count === 0 || count > 1 || count === undefined) return `${word}s`;
		else return word;
	}
	static formatDate(date) {
		if (!date) return '';
		var d = new Date(date);
		let day = d.getDate();
		let month = d.getMonth() + 1;
		if (day < 10) {
			day = '0' + day;
		}
		if (month < 10) {
			month = '0' + month;
		}
		return `${d.getFullYear()}-${month}-${day}`;
	}
}

export default Utils;
